package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter
{
    public static void main(String[] args)
    {
        System.out.println("Enter an Integer value: ");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        //Random rand = new Random();
        int result = Utilities.doubleMe(num);
        System.out.println(result);
    }
}